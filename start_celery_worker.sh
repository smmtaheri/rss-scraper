#! /usr/bin/env bash

while ! poetry run celery -A rss_scraper worker -c "${CELERY_WORKERS_COUNT}" -l info 2>&1; do
  sleep 3
done
