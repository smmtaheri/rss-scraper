FROM python:3.8

RUN pip install -U setuptools pip

WORKDIR /app

COPY pyproject.toml ./
RUN pip3.8 install poetry && poetry install
COPY ./ ./

RUN chmod +x /app/start_django.sh
RUN chmod +x /app/start_celery_beat.sh
RUN chmod +x /app/start_celery_worker.sh