# RSS-Scraper

1. Load environment variables in .env file.
2. Install last version of docker compose.
3. Run `docker-compose --build`. (5 services should be up: django, celery worker, celery beat, rabbitmq and mysql db)
4. If everything is up properly you can see the routes on port you set in your host.
