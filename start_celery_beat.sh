#! /usr/bin/env bash

while ! poetry run celery -A rss_scraper beat -l info 2>&1; do
  sleep 3
done