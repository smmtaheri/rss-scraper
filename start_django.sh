#poetry run python manage.py flush --no-input
while ! poetry run python manage.py collectstatic --noinput 2>&1; do
  sleep 3
done
while ! poetry run python manage.py flush --no-input 2>&1; do
  sleep 3
done
while ! poetry run python manage.py migrate  2>&1; do
   sleep 3
done
#poetry run python manage.py migrate
#poetry run python manage.py loaddata ./*/fixtures/*.json
export APP_MODULE=${DJANGO_PROJECT}.wsgi:application
poetry run gunicorn --bind 0.0.0.0:$DJANGO_WEB_PORT $APP_MODULE