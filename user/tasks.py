# poetry run celery -A rss_scraper worker -c 2 -l info
# poetry run celery -A rss_scraper beat -l info

import logging
from celery import shared_task

from tenacity import after_log, before_log, retry, stop_after_attempt, wait_fixed

from user.models import Feed
from utils.scraper import RssScrape

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


@shared_task
def scrape_feeds():
    feeds = Feed.objects.all()
    for feed in feeds:
        scrape_feed(feed.url)


@retry(
    stop=stop_after_attempt(10),
    wait=wait_fixed(1),
    before=before_log(logger, logging.INFO),
    after=after_log(logger, logging.WARN),
)
def scrape_feed(url):
    rs = RssScrape(url)
    rs.scrape()
    rs.parse()
    rs.save()
