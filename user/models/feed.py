from django.db import models

from user.models.base import TimestampedModel


class Feed(TimestampedModel):
    url = models.URLField(max_length=500)
    is_active = models.BooleanField(default=True)

    objects = models.Manager()
