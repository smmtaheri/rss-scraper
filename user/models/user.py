from django.db import models
from django.contrib.auth.models import AbstractUser

from user.models import Feed, FeedItem


class User(AbstractUser):
    feeds = models.ManyToManyField(Feed)
    feed_items = models.ManyToManyField(FeedItem)

    def __repr__(self):
        return f'ID: {self.pk}' \
               f'Feed: {self.feeds}' \
               f'Feed Items: {self.feed_items}'
