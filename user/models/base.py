from django.db import models
from datetime import datetime


class TimestampedModel(models.Model):
    # A timestamp representing when this object was last updated.
    updated_at = models.DateTimeField(auto_now=True, blank=True, null=True)

    # A timestamp representing when this object was created.
    created_at = models.DateTimeField(default=datetime.now)

    class Meta:
        abstract = True

        ordering = ['-created_at', '-updated_at']
