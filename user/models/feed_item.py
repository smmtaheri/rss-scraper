from datetime import datetime

from django.db import models

from user.models.base import TimestampedModel
from user.models.feed import Feed


class FeedItem(TimestampedModel):
    title = models.TextField()
    description = models.TextField()
    pub_date = models.DateTimeField(default=datetime.now)
    persian_pub_date = models.DateTimeField(default=datetime.now)
    link = models.URLField(max_length=4000)
    guid = models.URLField(max_length=200, unique=True)
    feed = models.ForeignKey(Feed, on_delete=models.CASCADE)

    objects = models.Manager()
