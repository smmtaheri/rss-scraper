from rest_framework import serializers

from .models import Feed, User, FeedItem


class MainSerializer(serializers.ModelSerializer):
    created_at = serializers.SerializerMethodField(method_name='get_created_at')
    updated_at = serializers.SerializerMethodField(method_name='get_updated_at')

    def get_created_at(self, instance):
        return instance.created_at.isoformat()

    def get_updated_at(self, instance):
        if instance.updated_at:
            return instance.updated_at.isoformat()


class FeedSerializer(MainSerializer):
    class Meta:
        model = Feed
        fields = '__all__'


class FeedItemSerializer(MainSerializer):
    class Meta:
        model = FeedItem
        fields = '__all__'


class FeedFollowSerializer(MainSerializer):
    feed_id = FeedSerializer(many=True, read_only=True, source='feed')

    class Meta:
        model = User
        fields = ['feed_id']


class UserSerializer(MainSerializer):
    feeds = FeedSerializer(read_only=True, many=True)
    feed_items = FeedItemSerializer(read_only=True, many=True)

    class Meta:
        model = User
        fields = ['feeds', 'feed_items']


class FeedItemFaveSerializer(MainSerializer):
    feed_item_id = FeedItemSerializer(many=True, read_only=True, source='feed_item')

    class Meta:
        model = User
        fields = ['feed_item_id']
