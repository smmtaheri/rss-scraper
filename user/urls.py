from django.urls import path
# from .views import RegisterView, ChangePasswordView, UpdateProfileView, LogoutView, LogoutAllView
# from rest_framework_simplejwt.views import TokenRefreshView, TokenObtainPairView
from user.views import CreateFeedAPIView, CreateFollowFeedAPIView, ListFeedFollowingAPIView, CreateFaveFeedItemAPIView, \
    ListFeedItemFaveAPIView

urlpatterns = [
    path('feed/create/', CreateFeedAPIView.as_view(), name='create_feed'),
    path('feed/follow/', CreateFollowFeedAPIView.as_view(), name='follow_feed'),
    path('feed/following/', ListFeedFollowingAPIView.as_view(), name='get_following_feed'),
    path('feed/item/fave/', CreateFaveFeedItemAPIView.as_view(), name='fave_feed_item'),
    path('feed/item/following/', ListFeedItemFaveAPIView.as_view(), name='fave_feed_item'),
]
