from django.test import TestCase
from user.models import Feed, FeedItem, User


class FeedItemTestCase(TestCase):
    def setUp(self):
        feed = Feed.objects.create(url='https://farsnews.ir/rss')
        FeedItem.objects.create(title='برگزاری اولین نشست شناسایی ساختمان های ناایمن در استان قزوین',
                                description='',
                                pub_date='2021-01-10 19:00:00',
                                persian_pub_date='2021-01-10 22:30:00',
                                link='https://farsnews.ir/news/14010316000690/%D8%A8%D8%B1%DA%AF%D8%B2%D8%A7%D8%B1%DB%',
                                guid='https://farsnews.ir/14010316000690',
                                feed=feed)

    def test_relation_to_feed(self):
        """
        Feed items are correctly related to feed or not.
        """
        feed_item = FeedItem.objects.get(guid='https://farsnews.ir/14010316000690')
        self.assertEqual(feed_item.feed.url, 'https://farsnews.ir/rss')


class UserTestCase(TestCase):
    def setUp(self):
        user = User.objects.create(username='username',
                                   password='Aa123456@F',
                                   email='u1@test.com',
                                   first_name='user1',
                                   last_name='user1')
        feed = Feed.objects.create(url='https://farsnews.ir/rss')
        feed_item = FeedItem.objects.create(title='برگزاری اولین نشست شناسایی ساختمان های ناایمن در استان قزوین',
                                            description='',
                                            pub_date='2021-01-10 19:00:00',
                                            persian_pub_date='2021-01-10 22:30:00',
                                            link='https://farsnews.ir/news/14010316000690/%D8%A8%D8%B1%DA%AF%D8%B2%D8%A7%D8%B1%DB%',
                                            guid='https://farsnews.ir/14010316000690',
                                            feed=feed)
        user.feeds.add(feed)
        user.feed_items.add(feed_item)

    def test_m_to_m_relation(self):
        """
        User is correctly related to feed and feed items or not.
        """
        user = User.objects.get(username='username')
        self.assertEqual(user.feeds.all()[0].url, 'https://farsnews.ir/rss')
        self.assertEqual(user.feed_items.all()[0].guid, 'https://farsnews.ir/14010316000690')
        self.assertEqual(user.feed_items.all()[0].feed.url, 'https://farsnews.ir/rss')
