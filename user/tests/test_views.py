from django.test import TestCase
from rest_framework.test import APIClient

from user.models import User


class CreateFeedAPIViewTest(TestCase):
    def setUp(self):
        self.api_client = APIClient()
        User.objects.create_user(username='u4', password='Aa123456@F', email='u4@test.com')

    def test_deny_anonymous_user(self):
        response = self.client.get('/api/V1/user/feed/create/')
        self.assertEqual(response.status_code, 401)

    def test_success(self):
        user = User.objects.get(username='u4')
        self.api_client.force_authenticate(user=user)
        response = self.api_client.post('/api/V1/user/feed/create/', {'url': 'https://www.rajanews.ir/rss'})
        self.assertEqual(response.status_code, 201)

    def test_fail_blank(self):
        user = User.objects.get(username='u4')
        self.api_client.force_authenticate(user=user)
        response = self.api_client.post('/api/V1/user/feed/create/', {})
        self.assertNotEqual(response.status_code, 201)
