from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from .models import Feed, FeedItem
from rest_framework.generics import ListAPIView
from rest_framework.generics import CreateAPIView
from .serializers import FeedSerializer, FeedFollowSerializer, FeedItemFaveSerializer, UserSerializer, \
    FeedItemSerializer


class CreateFeedAPIView(CreateAPIView):
    """
    This endpoint allows to insert a rss url.
    """
    serializer_class = FeedSerializer
    permission_classes = (IsAuthenticated,)


class CreateFollowFeedAPIView(CreateAPIView):
    """
    This endpoint allows to user for follow a rss feed.
    """
    permission_classes = (IsAuthenticated,)

    def create(self, request, *args, **kwargs):
        feed = Feed.objects.filter(pk=self.request.data.get('feed_id')).first()
        if feed:
            self.request.user.feeds.add(feed)
            return Response({"message": "User follow this feed successfully!"}, status=status.HTTP_201_CREATED)
        else:
            return Response({"message": "This feed does not exist!"}, status=status.HTTP_404_NOT_FOUND)


class ListFeedFollowingAPIView(ListAPIView):
    """
    This endpoint allows to user for get all following feeds.
    """
    serializer_class = FeedSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        return self.request.user.feeds.all()


class CreateFaveFeedItemAPIView(CreateAPIView):
    """
    This endpoint allows to user for fave a rss feed item.
    """
    permission_classes = (IsAuthenticated,)

    def create(self, request, *args, **kwargs):
        feed_item = FeedItem.objects.filter(pk=self.request.data.get('feed_item_id')).first()
        if feed_item:
            self.request.user.feed_items.add(feed_item)
            return Response({"message": "User fave this feed item successfully!"}, status=status.HTTP_201_CREATED)
        else:
            return Response({"message": "This feed item does not exist!"}, status=status.HTTP_404_NOT_FOUND)


class ListFeedItemFaveAPIView(ListAPIView):
    """
    This endpoint allows to user for get all fave feed items.
    """
    serializer_class = FeedItemSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        return self.request.user.feed_items.all()
