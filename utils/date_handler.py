import jdatetime
from datetime import datetime
from dateutil import parser


class DateTimeHandler:

    def __init__(self, date_time_string):
        self.__dt_string = date_time_string

    def parse(self):
        try:
            date_time_obj = parser.parse(self.__dt_string)
        except Exception:
            raise BadDateTime()
        if self.is_persian_date(date_time_obj.year):
            gregorian = jdatetime.date(date_time_obj.year, date_time_obj.month, date_time_obj.day).togregorian()
            date_time_obj = datetime(gregorian.year,
                                     gregorian.month,
                                     gregorian.day,
                                     date_time_obj.year,
                                     date_time_obj.month,
                                     date_time_obj.day)
        return date_time_obj.strftime('%Y-%m-%d %H:%M:%S')

    @staticmethod
    def is_persian_date(year):
        if year < 1500:
            return True
        return False


class BadDateTime(Exception):
    pass
