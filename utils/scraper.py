from abc import ABC
from re import compile
import requests
import arrow
import xml.etree.ElementTree as ET

from rss_scraper import settings
from user.models import FeedItem, Feed
from utils.date_handler import DateTimeHandler


class RequestBase(ABC):
    @staticmethod
    def get(*args, **kwargs):
        response = requests.get(
            *args, **kwargs, timeout=settings.REQUEST_TIME_OUT
        )
        if response.status_code != 200:
            response.raise_for_status()
        return response

    @staticmethod
    def post(*args, **kwargs):
        response = requests.post(
            *args, **kwargs, timeout=settings.REQUEST_TIME_OUT
        )
        if response.status_code != 200:
            response.raise_for_status()
        return response


class RssScrape(RequestBase):
    """
    This class is designed for scrap, parse and save items of a rss url.
    """

    def __init__(self, url: str):
        self.__url = url
        self.__rsp = None
        self.__parsed_response = dict()

    def scrape(self) -> None:
        self.__rsp = self.get(self.__url)

    def parse(self) -> None:
        """
        Convert response text to xml tree and then convert it to dict object.
        """
        try:
            xml_tree = ET.ElementTree(ET.fromstring(self.__rsp.text.strip()))
        except:
            xml_tree = ET.ElementTree(ET.fromstring(self.__rsp.content))

        main_items = xml_tree.findall('.//channel/*')
        news_items = xml_tree.findall('.//item')
        for item in main_items:
            if 'item' not in item.tag and item.text:
                self.__parsed_response[item.tag] = item.text.strip()
        items = list()
        for news_item in news_items:
            one_news = dict()
            for item in news_item.iter():
                if 'item' not in item.tag and item.text:
                    one_news[item.tag] = item.text.strip()
            self.__add_pub_date(one_news)
            self.__add_guid(one_news)
            items.append(one_news)
        self.__parsed_response['items'] = items

    def save(self) -> None:
        """
        Save rss items into FeedItem model.
        """
        feed_items_objs = []
        for item in self.__parsed_response.get('items'):
            feed = Feed.objects.filter(url=self.__url).first()
            feed_items_objs.append(FeedItem(title=item.get('title'),
                                            description=item.get('description'),
                                            pub_date=item.get('pub_date'),
                                            persian_pub_date=item.get('persian_pub_date'),
                                            link=item.get('link'),
                                            guid=item.get('guid'),
                                            feed=feed,
                                            ))
        FeedItem.objects.bulk_create(feed_items_objs, ignore_conflicts=True)

    @staticmethod
    def __add_pub_date(item: dict) -> None:
        """
        Add publish date into rss item.
        """
        date_key = None
        is_gmt = False
        for key, value in item.items():
            if 'pub' in key.lower() and 'date' in key.lower():
                if 'gmt' in value.lower():
                    is_gmt = True
                date_key = key
                break
        if date_key:
            date_value = DateTimeHandler(item.pop(date_key)).parse()
            item['pub_date'] = date_value
            if is_gmt:
                item['persian_pub_date'] = arrow.get(date_value).to('Asia/Tehran').format('YYYY-MM-DD HH:mm:ss')
            else:
                item['persian_pub_date'] = date_value

    @staticmethod
    def __add_guid(item: dict) -> None:
        """
        Add guid into rss item. guid is unique part of a rss url.
        """
        url = item.get('guid') or item.get('link')
        url_parts = url.split('/')
        url_sections = url_parts[3:]
        url_sections.reverse()
        pattern = compile(r'\d{2,}')
        guid = ''
        for sec in url_sections:
            if pattern.match(sec):
                guid = '/'.join(url_parts[:url_parts.index(sec) + 1])
        if guid:
            item['guid'] = guid
        else:
            item['guid'] = url
